import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    })
};



@Injectable({
  providedIn: 'root'
})
export class SaldoService {

	private url : any = "http://localhost:8080/saldo";
  private url2 : any = "http://localhost:8080/comprar";
  private url3 : any = "http://localhost:8080/cartola";

  constructor(
  	protected httpClient: HttpClient
  	) { }


  agregar_recarga(user_id, precio) {

    const body = new HttpParams()
      .set('producto', "Recarga")
      .set('tipo', "0")
      .set('precio', precio)
      .set('userId', user_id)
 
    return this.httpClient.post<any>(this.url, body.toString(), httpOptions);
  }


  comprar(user_id, product_id) {
    return this.httpClient.post<any>(this.url2+"/"+user_id+"/"+product_id, httpOptions);
  }

  getCartola(user_id){
  	return this.httpClient.get(this.url3+"/"+user_id, httpOptions);
  }

}


