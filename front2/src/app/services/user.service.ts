import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',

    })
};



@Injectable({
  providedIn: 'root'
})
export class UserService {

	private url : any = "http://localhost:8080/user";
  constructor(
  	protected httpClient: HttpClient
  	) { }


  cambiar_nombre(user_id, nombre) {

    const body = new HttpParams()
      .set('activo', "1")
      .set('nombre', nombre)
      .set('id', user_id)
 
    return this.httpClient.put(this.url, body);
  }
}


