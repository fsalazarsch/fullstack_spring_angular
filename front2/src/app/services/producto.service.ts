import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    })
};

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

	private url : any = "http://localhost:8080/producto";

  constructor(
  	protected httpClient: HttpClient
  	) { }



  getProductos(){
  	return this.httpClient.get(this.url, httpOptions);
  }

}


