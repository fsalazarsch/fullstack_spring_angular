import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgregarSaldoComponent } from './saldo/agregar.component';
import { EditarNombreComponent } from './nombre/editar.component';
import { HistoricoComponent } from './historico/listar.component';
import { ComprarComponent } from './comprar/listar.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    AgregarSaldoComponent,
    EditarNombreComponent,
    HistoricoComponent,
    ComprarComponent ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
