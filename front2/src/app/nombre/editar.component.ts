import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarNombreComponent implements OnInit {

	public data : any;
	public last : any;
	public paramspage : any;

  constructor(
  	private activeRouter: ActivatedRoute,
  	protected userService : UserService
  	) { }

  ngOnInit(): void {
  }

    onSubmit(form: { value: { user_id: number; nombre: string; }; }) {
        this.userService.cambiar_nombre(form.value.user_id, form.value.nombre).subscribe(async res => {
        }, (error) => {
        });

    	alert("Cambiado con exito");
    	location.reload();
    }



}
