import { Component, OnInit } from '@angular/core';
import { SaldoService } from 'src/app/services/saldo.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class HistoricoComponent implements OnInit {

  public data : any;
  public total : any;

  constructor(
  	private activeRouter: ActivatedRoute,
  	protected saldoService : SaldoService
  	) { }

  ngOnInit(): void {
  }

    onSubmit(form: { value: { user_id: number; nombre: string; }; }) {
        this.saldoService.getCartola(form.value.user_id).subscribe(async res => {
          this.data = res;

          this.total = this.data.reduce(function(sum, record) {
             return (record.tipo !== 0) ? sum - record.precio : sum + record.precio;
           }, 0);

          console.log(res);
        }, (error) => {
        });

    	//alert("Cambiado con exito");
    	//location.reload();
    }



}
