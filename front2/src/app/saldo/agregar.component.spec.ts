import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarSaldoComponent } from './agregar.component';

describe('AgregarSaldoComponent', () => {
  let component: AgregarSaldoComponent;
  let fixture: ComponentFixture<AgregarSaldoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarSaldoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarSaldoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
