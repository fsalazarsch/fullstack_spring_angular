import { Component, OnInit } from '@angular/core';
import { SaldoService } from 'src/app/services/saldo.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarSaldoComponent implements OnInit {

	public data : any;
	public last : any;
	public paramspage : any;

  constructor(
  	private activeRouter: ActivatedRoute,
  	protected saldoService : SaldoService
  	) { }

  ngOnInit(): void {

  }

    onSubmit(form: { value: { user_id: number; precio: number; }; }) {
        this.saldoService.agregar_recarga(form.value.user_id, form.value.precio).subscribe(async res => {
        }, (error) => {
        });

    	alert("Recargado con exito");
    	location.reload();
    }


}
