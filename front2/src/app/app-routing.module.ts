import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgregarSaldoComponent } from './saldo/agregar.component';
import { EditarNombreComponent } from './nombre/editar.component';
import { HistoricoComponent } from  './historico/listar.component';
import { ComprarComponent } from './comprar/listar.component';

const routes: Routes = [
{ path: '',   redirectTo: '/', pathMatch: 'full' }, 
{ path: 'saldo',  component: AgregarSaldoComponent },
{ path: 'editar_nombre',  component: EditarNombreComponent },
{ path: 'historico',  component: HistoricoComponent },
{ path: 'comprar',  component: ComprarComponent }
  ]
 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

