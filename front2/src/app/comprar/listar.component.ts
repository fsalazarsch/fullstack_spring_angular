import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/services/producto.service';
import { SaldoService } from 'src/app/services/saldo.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ComprarComponent implements OnInit {

  public data : any;
  public total : any;

  constructor(
  	private activeRouter: ActivatedRoute,
    protected productService : ProductoService,
    protected saldoService : SaldoService
  	) { }

  ngOnInit(): void {
    this.activeRouter.queryParams.subscribe(params => {

     this.productService.getProductos().subscribe((data: any) => {
        
        this.data = data;
        
        console.log(this.data);

      }, (error: any) => {
        console.error(error);
      }, () => {});

    });
  }

    onSubmit(form: { value: { user_id: number; product_id: string; }; }) {
        this.saldoService.comprar(form.value.user_id, form.value.product_id).subscribe(async res => {
          
          console.log(res);
          if (res.msg == "error")
            alert("No posees suficiente saldo");
          else
            alert("Comprado con exito");

        }, (error) => {
        });

    	//alert("Cambiado con exito");
    	//location.reload();
    }



}
