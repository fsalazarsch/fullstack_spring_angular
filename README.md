# README #


### Desglose del repositorio ###

* El repositorio consta de 2 partes
* Backend desarrollado en spring boot
* Frontend desarrollado en angular 8

### Setup ###

* El backend se ejecuta como una aplicacion Spring
* El front como aplicacion angular, usando ng-serve
* El puerto para la api es el 8080 y el del front 4200, la api esta configurada para poder recbir peticiones de  este puerto
* La base de datos configurada con estructura y datos es eventos, el archivo esta adjunto en el repositorio y es un MySQL


### Consideraciones ###

* La api esta testeada para funcionar con las funciones requeridas
* La base de la api contiene los 2 ejercicios primarios
* La base del front tiene un enlace a cada una de las dunciones requeridas
