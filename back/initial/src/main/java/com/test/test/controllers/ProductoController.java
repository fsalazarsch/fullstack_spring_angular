package com.test.test.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.test.entity.models.Producto;
import com.test.test.entity.services.IProductoService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ProductoController {

	@Autowired
	IProductoService productoservice;

	@GetMapping("/producto")
	public List<Producto> getAll() {
		return productoservice.getAll();
	}
	
	@GetMapping("/producto/{id}")
	public Producto getOne(@PathVariable(value = "id") String id ) {
		return productoservice.get(id);
	}
	
	@PostMapping("/producto")
	public void add(Producto producto) {
		productoservice.post(producto);
	}
	
	@PutMapping("/producto")
	public void update(Producto producto, String id) {
		productoservice.put(producto, id);
	}
	
	@DeleteMapping("/producto/{id}")
	public void delete(String id) {
		productoservice.delete(id);
	}
}

