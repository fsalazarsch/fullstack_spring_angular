package com.test.test.entity.services;

import java.util.List;

import com.test.test.entity.models.Saldo;

public interface ISaldoService {

	public Saldo get(long id);
	public List<Saldo> getAll();
	public List<Saldo> getCartola(long userid);
	public String comprar(long userid, String id_producto);
	public void post(Saldo saldo);
	public void put(Saldo saldo, Long id);
	public void delete(long id);
	
}
