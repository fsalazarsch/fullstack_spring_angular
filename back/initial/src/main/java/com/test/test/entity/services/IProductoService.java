package com.test.test.entity.services;

import java.util.List;

import com.test.test.entity.models.Producto;

public interface IProductoService {

	public Producto get(String id);
	public List<Producto> getAll();
	public void post(Producto producto);
	public void put(Producto producto, String id);
	public void delete(String id);
	
}
