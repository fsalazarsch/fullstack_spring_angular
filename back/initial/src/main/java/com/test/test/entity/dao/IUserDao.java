package com.test.test.entity.dao;

import org.springframework.data.repository.CrudRepository;

import com.test.test.entity.models.User;

public interface IUserDao extends CrudRepository<User, Long> {

}
