package com.test.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {


   
	@RequestMapping("/")
	public String home() {
	    String res ="";
		int[] arreglo = new int[]{1, 3, 13, 15, 17, 19, 21, 23, 31, 34, 40, 51, 54, 68}; 
	    res += buscarv(arreglo, 51) +"<hr>";
	    int[] arreglo2 = new int[]{54,53,2,1,5,98,73,86,98,94,1,2,3,2};
	    res += bubblesort(arreglo2);
	    
		return res;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	  public static String buscarv( int[] arreglo, int valor){
		  String ret = "";
		    for(int i =0; i < arreglo.length; i++){
		      if(arreglo[i] == valor)
		        ret += "El valor "+valor+" fue encontrado en la posicion "+ (i+1)+"<br>";
		    }
		    return ret;
		  }

		  public static String bubblesort(int[] arreglo){
		      String ret = "";
			  int i, j, aux;		      
		      
		      for(i = 0; i < arreglo.length-1; i++)
		        for(j= i; j < arreglo.length; j++){
		          if (arreglo[i] > arreglo[j]){
		            aux = arreglo[i];
		            arreglo[i] = arreglo[j];
		            arreglo[j] = aux;
		          }

		        }

		      for(i=0; i< arreglo.length; i++){
		        ret += arreglo[i] + " | ";
		      }
		      return ret;
		  }

}

