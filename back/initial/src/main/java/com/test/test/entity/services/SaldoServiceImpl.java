package com.test.test.entity.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.test.entity.models.Producto;
import com.test.test.entity.dao.ISaldoDao;
import com.test.test.entity.models.Saldo;
import com.test.test.entity.dao.IProductoDao;

@Service
public class SaldoServiceImpl implements ISaldoService{
	
	@Autowired
	private ISaldoDao saldoDao;
	@Autowired
	private IProductoDao productoDao;

	@Override
	public Saldo get(long id) {
		return saldoDao.findById(id).get();
	}

	@Override
	public List<Saldo> getAll() {
		return (List<Saldo>) saldoDao.findAll();
	}

	@Override
	public void post(Saldo saldo) {
		saldoDao.save(saldo);
		
	}

	@Override
	public void put(Saldo saldo, Long id) {
		saldoDao.findById(id).ifPresent((x)->{
			saldo.setId(id);
			saldoDao.save(saldo);
		} );
		
	}

	@Override
	public void delete(long id) {
		saldoDao.deleteById(id);
		
	}


	@Override
	public List<Saldo> getCartola(long userid) {
	    
		List<Saldo> filteredList = new ArrayList<>();
	    List<Saldo> originalList = (List<Saldo>) saldoDao.findAll();
		
	    for (Saldo saldo : originalList) {
	    	if(saldo.getUserId() == userid)
	    		filteredList.add(saldo);
	    	
	    }
		
		return filteredList;
		
	}

	@Override
	public String comprar(long userid, String id_producto) {
	    
	    List<Saldo> originalList = (List<Saldo>) saldoDao.findAll();
		int total = 0;
	    
	    for (Saldo saldo : originalList) {
	    	if(saldo.getUserId() == userid) {
	    		if(saldo.getTipo() == 0)
	    			total += saldo.getPrecio();
	    		if(saldo.getTipo() == 1)
	    			total -= saldo.getPrecio();
	    	}
	    }
		
	    Producto p = productoDao.findById(id_producto).get();
	    if( total >= p.getPrecio()) {
	    	Saldo saldo2 = new Saldo();
	    	saldo2.setPrecio(p.getPrecio());
	    	saldo2.setProducto(p.getNombre());
	    	saldo2.setTipo(1);
	    	saldo2.setUserId(userid);
	    	saldoDao.save(saldo2);
	    	return "{\"msg\" : \"OK\"}";
	    }
	    return "{\"msg\" : \"error\"}";
		
	}


}
