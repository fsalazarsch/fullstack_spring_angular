package com.test.test.entity.dao;

import org.springframework.data.repository.CrudRepository;

import com.test.test.entity.models.Saldo;

public interface ISaldoDao extends CrudRepository<Saldo, Long> {

}
