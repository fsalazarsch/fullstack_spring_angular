package com.test.test.entity.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.test.entity.dao.IProductoDao;
import com.test.test.entity.models.Producto;

@Service
public class ProductoServiceImpl implements IProductoService{
	
	@Autowired
	private IProductoDao productoDao;
	
	@Override
	public Producto get(String id) {
		return productoDao.findById(id).get();
	}

	@Override
	public List<Producto> getAll() {
		return (List<Producto>) productoDao.findAll();
	}

	@Override
	public void post(Producto producto) {
		productoDao.save(producto);
		
	}

	@Override
	public void put(Producto producto, String id) {
		productoDao.findById(id).ifPresent((x)->{
			producto.setId(id);
			productoDao.save(producto);
		} );
		
	}

	@Override
	public void delete(String id) {
		productoDao.deleteById(id);
		
	}

}


