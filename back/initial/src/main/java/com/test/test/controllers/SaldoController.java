package com.test.test.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.test.entity.models.Saldo;
import com.test.test.entity.services.ISaldoService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class SaldoController {

	@Autowired
	ISaldoService saldoservice;

	@GetMapping("/saldo")
	public List<Saldo> getAll() {
		return saldoservice.getAll();
	}
	
	@GetMapping("/saldo/{id}")
	public Saldo getOne(@PathVariable(value = "id") long id ) {
		return saldoservice.get(id);
	}

	@GetMapping("/cartola/{user_id}")
	public List<Saldo> getCartola(@PathVariable(value = "user_id") int user_id) {
		return saldoservice.getCartola(user_id);
	}
	
	@PostMapping("/comprar/{user_id}/{id_producto}")
	public String comprar(@PathVariable(value = "user_id") int user_id, @PathVariable(value = "id_producto") String id_producto) {
		return saldoservice.comprar(user_id, id_producto);
		
	}
	
	@PostMapping("/saldo")
	public void add(Saldo saldo) {
		saldoservice.post(saldo);
	}
	
	@PutMapping("/saldo")
	public void update(Saldo saldo, long id) {
		saldoservice.put(saldo, id);
	}
	
	@DeleteMapping("/saldo/{id}")
	public void delete(long id) {
		saldoservice.delete(id);
	}
}