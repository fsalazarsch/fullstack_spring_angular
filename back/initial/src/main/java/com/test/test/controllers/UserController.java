package com.test.test.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.test.entity.models.User;
import com.test.test.entity.services.IUserService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {

	@Autowired
	IUserService userService;

	@GetMapping("/user")
	public List<User> getAllUsers() {
		return userService.getAll();
	}
	
	@GetMapping("/user/{id}")
	public User getOne(@PathVariable(value = "id") Long id ) {
		return userService.get(id);
	}
	
	@PostMapping("/user")
	public void add(User user) {
		userService.post(user);
	}
	
	@PutMapping("/user")
	public void update(User user, Long id) {
		userService.put(user, id);
	}
	
	@DeleteMapping("/user/{id}")
	public void delete(long id) {
		userService.delete(id);
	}
}
