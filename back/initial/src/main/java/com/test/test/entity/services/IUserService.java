package com.test.test.entity.services;

import java.util.List;

import com.test.test.entity.models.User;


public interface IUserService {

	public User get(long id);
	public List<User> getAll();
	public void post(User user);
	public void put(User user, Long id);
	public void delete(long id);
}
