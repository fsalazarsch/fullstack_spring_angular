package com.test.test.entity.dao;

import org.springframework.data.repository.CrudRepository;

import com.test.test.entity.models.Producto;

public interface IProductoDao extends CrudRepository<Producto, String> {

}

